package de.group6.graphnetworkmodel.datatypes;

import java.util.LinkedList;

/**
 * Class which represents a vertex in our graph modeling.
 * 
 * @author JavaGroup6
 */
class Vertex extends Node implements Comparable<Vertex> {

	/**
	 * Previous vertex is used to get path in Dijkstra.
	 */
	private Vertex prevVertex;

	/**
	 * Edge list in order to maintain the adjacent vertices of a vertex and 
	 * to get the distance to the adjacent vertices.
	 */
	private LinkedList<Edge> edgeList;

	/**
	 * Current distance of a vertex to a start vertex which
	 * is calculated when applying the Dijkstra algorithm.
	 */
	private int distance;

	/**
	 * Colour which is used in the connectivity check order to indicate if a vertex
	 * is visited or not. It is used as break condition for the recursion.
	 */
	private Colour vColour;

	/**
	 * Number which indicates the insertion order of a vertex. It is used to increase
	 * performance in methods which require the Floyd algorithm.
	 */
	private int insertionOrder;

	/**
	 * Constructor, which initializes the attributes of a vertex.
	 */
	Vertex(String nameId) {
		
		super(nameId);
		edgeList = new LinkedList<Edge>();
		distance = Integer.MAX_VALUE;
		prevVertex = null;
		vColour = Colour.WHITE;
	}
	
	/**
	 * compareTo method, vertices will be compared by their distances. 
	 */
	@Override
	public int compareTo(Vertex o) {
		
		if (this.getDistance() > o.getDistance())
			return 1;
		else if (this.getDistance() < o.getDistance())
			return -1;
		else
			return 0;
	}

	Vertex getPrevVertex() {
		
		return prevVertex;
	}

	LinkedList<Edge> getEdgeList() {
		
		return edgeList;
	}

	int getDistance() {
		
		return distance;
	}

	Colour getColour() {
		
		return vColour;
	}

	int getInsertionOrder() {
		
		return insertionOrder;
	}
	
	int getAmountEdges() {
		
		return edgeList.size();
	}
	
	void setPrevVertex(Vertex prevVertex) {
		
		this.prevVertex = prevVertex;
	}

	void setDistance(int distance) {
		
		this.distance = distance;
	}

	void setColour(Colour colour) {
		
		this.vColour = colour;
	}

	void setInsertionOrder(int insertionOrder) {
		
		this.insertionOrder = insertionOrder;
	}

	void setEdgeList(LinkedList<Edge> edgeList) {
		
		this.edgeList = edgeList;
	}

	/**
	 * Method to add an edge into the adjacency list of a vertex.
	 * 
	 */
	void addEdge(Edge newEdge) {
		edgeList.add(newEdge);
	}
}