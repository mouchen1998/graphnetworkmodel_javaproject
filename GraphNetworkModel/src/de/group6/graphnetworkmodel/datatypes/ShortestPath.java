package de.group6.graphnetworkmodel.datatypes;

/**
 * Class which is used to model information of one existing shortest path
 * of a network graph object. It contains information about the start vertex, end vertex,
 * the path as String representation and the path length.
 * 
 * @author JavaGroup6
 */
 class ShortestPath {
	
	private Vertex source;
	private Vertex target;
	private String path;
	private int length;
	
	/**
	 * Constructor gets source vertex, target vertex, the path as String and length.
	 * 
	 * @param source
	 * @param target
	 * @param path
	 * @param length
	 */
	ShortestPath(Vertex source, Vertex target, String path, int length) {
		
		this.source = source;
		this.target = target;
		this.path = path;
		this.length = length;
	}
	
	/**
	 * @return The path in String representation
	 */
	@Override
	public String toString() {
		
		return path;
	}
	
	Vertex getSource() {
		
		return source;
	}

	Vertex getTarget() {
		
		return target;
	}

	String getPath() {
		
		return path;
	}

	int getLength() {
		
		return length;
	}

	void setSource(Vertex source) {
		
		this.source = source;
	}

	void setTarget(Vertex target) {
		
		this.target = target;
	}

	void setPath(String path) {
		
		this.path = path;
	}

	void setLength(int length) {
		
		this.length = length;
	}
}