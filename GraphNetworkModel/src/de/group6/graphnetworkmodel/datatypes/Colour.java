package de.group6.graphnetworkmodel.datatypes;

/**
 * Colours which represent different states of a vertex during the traversal
 * process in order to check the connectivity: 
 * WHITE: Initial state of a vertex
 * GREY: Vertex has been visited, but not fully processed since adjacent vertices have not been processed.
 * BLACK: Vertex is fully processed.
 * 
 * @author JavaGroup6
 */
enum Colour {
	 
	WHITE, GREY, BLACK
}