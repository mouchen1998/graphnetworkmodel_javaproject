package de.group6.graphnetworkmodel.datatypes;

/**
 * Interface which is used to implement our NetworkGraph class.
 * The following methods can be seen as blueprint of a basic graph class.
 * 
 * @author JavaGroup6
 */
public interface Graph {

	public int getNumberOfVertices();

	public int getNumberOfEdges();
	
	public void addVertex(String vertexNameId); 
	
	public void addEdge(String sourceNameId, String targetNameId, int edgeId, int weight);

	public void print();
}