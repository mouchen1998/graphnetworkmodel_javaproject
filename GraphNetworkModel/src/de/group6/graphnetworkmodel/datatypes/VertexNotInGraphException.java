package de.group6.graphnetworkmodel.datatypes;

import java.util.LinkedHashMap;
import java.util.Scanner;
import java.util.logging.Logger;

/**
 * Exception which handles the case if one tries to access a vertex which
 * does not exist in network graph object.
 * 
 * @author JavaGroup6
 */
class VertexNotInGraphException extends Exception {

	private final static Logger exceptionLOG = Logger.getLogger(VertexNotInGraphException.class.getName());
	
	VertexNotInGraphException() {
		
	}

	@Override 
	public String getMessage() {
		
		return "Error, vertex does not exist in graph!";
	}

	/**
	 * Method which is used to handle the case if user passes a non existing vertex.
	 * The user is asked to provide a new vertex nameId.
	 * 
	 * @return Returns a reference to an existing vertex if user enters valid vertex nameId
	 */
	@SuppressWarnings("resource")
	Vertex askForVertex(NetworkGraph networkGraph) {
		
		LinkedHashMap<String, Vertex> vertices = networkGraph.getVertices();
		Vertex vertex = null;
		String vertexKey = null;
		Scanner userInput = new Scanner(System.in);
		do {
			System.out.print("Please provide a new vertex nameId or terminate the program with 'exit'> ");
			vertexKey = userInput.nextLine();
			if (vertexKey.equalsIgnoreCase("exit")) {
				exceptionLOG.warning("Terminating program...");
				System.exit(1);
			}
			vertex = vertices.get(vertexKey);
		} while (!vertices.containsKey(vertexKey));
		return vertex;
	}
}