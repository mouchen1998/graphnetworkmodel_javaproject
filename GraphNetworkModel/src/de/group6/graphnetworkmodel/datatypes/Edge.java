package de.group6.graphnetworkmodel.datatypes;

/**
 * Class for modeling an edge of a network graph object.
 * 
 * @author JavaGroup6
 */
 class Edge implements Comparable<Edge>{

	private int edgeId;
	private int weight;
	private Vertex target;
	private Vertex source;
	
	/**
	 * Constructor is getting edgeId, weight, source vertex, target vertex.
	 * 
	 * @param edgeId
	 * @param weight
	 * @param target
	 * @param source
	 */
	Edge(int edgeId, int weight, Vertex source, Vertex target) {

		this.edgeId = edgeId;
		this.weight = weight;
		this.target = target;
		this.source = source;
	}

	/**
	 * The toString() method returns the edgeId of an edge object in String representation.
	 */
	@Override
	public String toString() {
		
		return String.valueOf(edgeId);
	}

	/**
	 * Two edges have the same hashcode if they are equal.
	 */
	@Override
	public int hashCode() {
		
		final int prime = 31;
		int result = 1;
		result = prime * result + ((source == null) ? 0 : source.hashCode());
		result = prime * result + ((target == null) ? 0 : target.hashCode());
		return result;
	}
	
	/**
	 * Two logical edges of different networkGraph objects are considered as equal if they have:
	 * Same edgeId, same sourceNameId, same targetNameId and the same weight.
	 */
	@Override
	public boolean equals(Object obj) {
	
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Edge other = (Edge) obj;
		if (this.getEdgeId()!=other.getEdgeId()) 
			return false;
		if (this.getWeight()!=other.getWeight()) 
			return false;
		if (source == null) {
			if (other.source != null)
				return false;
		} 
		else if (!source.equals(other.source))
			return false;
		if (target == null) {
			if (other.target != null)
				return false;
		} 
		else if (!target.equals(other.target))
			return false;
		return true;
	}
	
	/**
	 * compareTo method, edges will be compared by their EdgeIds. 
	 */
	@Override
	public int compareTo(Edge o) {
		
		if (this.getEdgeId() > o.getEdgeId())
			return 1;
		else if (this.getEdgeId() < o.getEdgeId())
			return -1;
		else
			return 0;
	}
	
	int getEdgeId() {
	
		return edgeId;
	}

	int getWeight() {
		
		return weight;
	}

	Vertex getSource() {
		
		return source;
	}
	
	Vertex getTarget() {
		
		return target;
	}

	void setEdgeId(int edgeId) {
		
		this.edgeId = edgeId;
	}

	void setWeight(int weight) {
		
		this.weight = weight;
	}

	void setTarget(Vertex target) {
		
		this.target = target;
	}

	void setSource(Vertex source) {
		
		this.source = source;
	}
}