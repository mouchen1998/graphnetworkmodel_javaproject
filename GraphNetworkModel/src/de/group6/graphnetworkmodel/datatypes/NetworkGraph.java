package de.group6.graphnetworkmodel.datatypes;

import de.group6.graphnetworkmodel.datatypes.Colour;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.HashSet;
import java.util.Stack;
import java.util.PriorityQueue;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.util.NoSuchElementException;
import java.io.IOException;
import java.util.logging.Logger;

/**
 * Implementation of an undirected graph which is used to model a network graph
 * structure.
 * 
 * @author JavaGroup6
 */
public class NetworkGraph implements Graph {

	private final static Logger networkGraphLOG = Logger.getLogger(NetworkGraph.class.getName());
	private int numberOfVertices;
	private int numberOfEdges;
	
	/**
	 * This data structure is used to save the references to unique edges of
	 * a network graph.
	 */
	private PriorityQueue<Edge> graphEdges;

	/**
	 * In this <key,value> data structure the graph saves its vertices.
	 */
	private LinkedHashMap<String, Vertex> vertices;

	/**
	 * All shortest paths will be save in a linked list, this list shall decrase
	 * overhead when calculating the shortest paths for file writing and printing of
	 * graph properties on the output stream.
	 */
	private LinkedList<ShortestPath> shortestPaths;

	/**
	 * Standard constructor initializes variables and creates the vertices
	 * LinkedHashMap.
	 */
	public NetworkGraph() {
		
		numberOfVertices = 0;
		numberOfEdges = 0;
		vertices = new LinkedHashMap<String, Vertex>();
		graphEdges = new PriorityQueue<Edge>();
		shortestPaths = null;
	}

	/**
	 * Copy constructor which creates a deep copy of a NetworkGraph object.
	 * 
	 * @param other NetworkGraph object from which a deep copy is made
	 */
	public NetworkGraph(NetworkGraph other) {

		this.vertices = new LinkedHashMap<String, Vertex>();
		this.numberOfVertices = 0;
		this.numberOfEdges = other.getNumberOfEdges();
		// Adding vertices
		for (Entry<String, Vertex> entry : other.vertices.entrySet()) {
			Vertex vertex = entry.getValue();
			Vertex newVertex = new Vertex(vertex.getNameId());
			this.addVertex(newVertex);
		}
		// Adding edges
		try {
			for (Entry<String, Vertex> entry : other.vertices.entrySet()) {
				Vertex vertex = entry.getValue();
				String sourceNameId = vertex.getNameId();
				Vertex sourceVertex = this.getVertex(sourceNameId);
				for (Edge edge : vertex.getEdgeList()) {
					String targetNameId = edge.getTarget().getNameId();
					int edgeId = edge.getEdgeId();
					int edgeWeight = edge.getWeight();
					Vertex targetVertex = this.getVertex(targetNameId);
					Edge newEdge = new Edge(edgeId,edgeWeight,sourceVertex,targetVertex);
					sourceVertex.addEdge(newEdge);
				}
			}
		} 
		catch (VertexNotInGraphException vertexNotInGraphException) {
			vertexNotInGraphException.printStackTrace();
		}
	}

	@Override
	public int getNumberOfVertices() {

		return numberOfVertices;
	}

	@Override
	public int getNumberOfEdges() {

		return numberOfEdges;
	}

	/**
	 * Method to add a vertex into a network graph object via the nameId. If the
	 * unique vertexNameId is already in the graph, the vertex will not be added and
	 * an error message will appear.
	 * 
	 * @param vertexNameId
	 */
	@Override
	public void addVertex(String vertexNameId) {

		try {
			if (vertexNameId == null)
				throw new NullPointerException("Passed vertexNameId is null, vertex will not be added!");
			Vertex newVertex = new Vertex(vertexNameId);
			addVertex(newVertex);
		} 
		catch (IllegalArgumentException illegalArgument) {
			illegalArgument.printStackTrace();
			System.err.println("You tried to insert a vertex which nameId is already assigned!");
		} 
		catch (NullPointerException nullPointerException) {
			nullPointerException.printStackTrace();
			System.err.println("NullPointerException occured!");
		}
	}

	/**
	 * This method adds an undirected edge to a network graph object. If the target or source
	 * vertex do not exist,it is tried to insert a loop, or the passed weight is negative
	 * an error message will occur with the corresponding edgeId. This edge will not be inserted.
	 */
	@Override
	public void addEdge(String sourceNameId, String targetNameId, int edgeId, int weight) {

		Vertex sourceVertex;
		Vertex targetVertex;
		try {
			if (sourceNameId == null || targetNameId == null)
				throw new NullPointerException("Added sourceNameId or targetNameId is null!");
			if (sourceNameId.equals(targetNameId))
				throw new IllegalArgumentException(
						"Loops are not allowed ! Edge with id " + edgeId + " has not been inserted.");
			sourceVertex = this.getVertex(sourceNameId);
			targetVertex = this.getVertex(targetNameId);
			LinkedList<Edge> tempEdgeList = sourceVertex.getEdgeList();
			if (tempEdgeList.contains(new Edge(edgeId, weight, sourceVertex, targetVertex)))
				throw new IllegalArgumentException(
						"Edge with the id " + edgeId + " exists already in the network graph!");
			if (weight < 0)
				throw new IllegalArgumentException("Edge with the id " + edgeId + " is negative !");
			Edge sourceEdge = new Edge(edgeId, weight,sourceVertex,targetVertex);
			Edge targetEdge = new Edge(edgeId, weight,targetVertex,sourceVertex);
			sourceVertex.addEdge(sourceEdge);
			targetVertex.addEdge(targetEdge);
			graphEdges.add(sourceEdge);
			numberOfEdges++;
		} 
		catch (VertexNotInGraphException vertexNotInGraphException) {
			vertexNotInGraphException.printStackTrace();
			if (!vertices.containsKey(targetNameId) && vertices.containsKey(sourceNameId))
				System.err.println("The target vertex with nameId: " + targetNameId + " does not exist");
			else if (vertices.containsKey(targetNameId) && !vertices.containsKey(sourceNameId))
				System.err.println("The source vertex with nameId: " + sourceNameId + " does not exist");
			else {
				System.err.println("The target vertex with nameId: " + targetNameId + " does not exist");
				System.err.println("The source vertex with nameId: " + sourceNameId + " does not exist");
			}
			System.err.println("Edge with id: " + edgeId + " has not been inserted!");
		} 
		catch (IllegalArgumentException illegalArgument) {
			illegalArgument.printStackTrace();
			System.err.println("Edge will not be added to the network graph !");
		} 
		catch (NullPointerException nullPointerException) {
			nullPointerException.printStackTrace();
		}
	}

	/**
	 * Method which applies Dijkstra and tracing of the searched shortest path.
	 * 
	 * @param startNameId The start vertex as String representation from which
	 *                    Dijkstra will be applied on
	 * @param endNameId   The destination vertex as String representation which is
	 *                    used to iterate over the previous vertices
	 * @return The built shortest path as String if graph is connected, else the
	 *         String will be empty
	 */
	public String getShortestPath(String startNameId, String endNameId) {

		if (!vertices.isEmpty()) {
			try {
				Vertex startVertex = this.getVertex(startNameId);
				Vertex endVertex = this.getVertex(endNameId);
				dijkstra(this, startVertex);
				String shortestPath = this.tracePath(startVertex, endVertex);
				if (endVertex.getDistance() == Integer.MAX_VALUE)
					return "";
				return shortestPath;
			} 
			catch (VertexNotInGraphException vertexNotInGraph) {
				vertexNotInGraph.printStackTrace();
				System.err.println(vertexNotInGraph.getMessage());
				Vertex startVertex;
				Vertex endVertex;
				if (!vertices.containsKey(startNameId)) {
					System.out.println("Start vertex is missing!");
					startVertex = vertexNotInGraph.askForVertex(this);
				} 
				else
					startVertex = vertices.get(startNameId);
				if (!vertices.containsKey(endNameId)) {
					System.out.println("End vertex is missing!");
					endVertex = vertexNotInGraph.askForVertex(this);
				} 
				else
					endVertex = vertices.get(endNameId);
				return getShortestPath(startVertex.getNameId(), endVertex.getNameId());
			}
		} 
		else
			return "";
	}

	/**
	 * This method checks the connectivity of a network graph object.
	 * 
	 * @return Returns true if graph is connected and false if not
	 */
	public synchronized boolean isConnected() {

		int amountOfVisitedVertices = 0;
		try {
			this.initConnectivity();
			Entry<String, Vertex> firstEntry = vertices.entrySet().iterator().next();
			Vertex vertex = vertices.get(firstEntry.getKey());
			amountOfVisitedVertices = visitVertex(vertex, 0);
			networkGraphLOG.info("Visited vertices: " + amountOfVisitedVertices);
		} 
		catch (NoSuchElementException emptyNetworkGraph) {
			emptyNetworkGraph.printStackTrace();
			System.err.println("Connectivity is undefined for an empty graph!");
			System.exit(1);
		}
		if (amountOfVisitedVertices == getNumberOfVertices())
			return true;
		else
			return false;
	}

	/**
	 * @return Returns the calculated diameter of the network graph object
	 */
	public int calculateDiameter() {

		int distMat[][] = new int[numberOfVertices][numberOfVertices];
		this.floyd(distMat);
		int diameter = 0;
		// Lower triangular matrix without diagonal
		for (int j = 0; j < numberOfVertices - 1; j++) {
			for (int i = (j + 1); i < numberOfVertices; i++) {
				if (distMat[i][j] > diameter)
					diameter = distMat[i][j];
			}
		}
		return diameter;
	}

	/**
	 * Method which the betweenness centrality of a vertex can be obtained.
	 * 
	 * @param vertexNameId The unique vertex nameId from which we want to get the
	 *                     betweenness centrality
	 * @return Returns the betweenness centrality of a vertex if vertex is not empty
	 */
	public double betweennessCentrality(String vertexNameId) {

		if (vertices.isEmpty()) {
			System.err.println("Graph does not have any vertices! ");
			System.exit(1);
		}
		try {
			Vertex vertex = this.getVertex(vertexNameId);
			int insertionOrder = vertex.getInsertionOrder();
			int V = this.getNumberOfVertices();
			int distMat[][] = new int[V][V];
			int countMat[][] = new int[V][V];
			calculateTables(distMat, countMat);
			return this.calculateBetweennessCentrality(distMat, countMat, insertionOrder);
		} 
		catch (VertexNotInGraphException vertexNotInGraph) {
			vertexNotInGraph.printStackTrace();
			Vertex vertex = vertexNotInGraph.askForVertex(this);
			return betweennessCentrality(vertex.getNameId());
		}
	}

	/**
	 * Method which is used to print the graph properties on the console. Calculates
	 * always all possible shortest paths in a new created LinkedList which contains
	 * ShortestPath objects.
	 */
	public void print() {

		if (!vertices.isEmpty())
			print(true);
	}

	/**
	 * Method to write all graph properties into a graphml file. Calculates always
	 * all possible shortest paths in a new created LinkedList which contains
	 * ShortestPath objects.
	 * 
	 * @param outputFilePath
	 */
	public void writeIntoFile(String outputFilePath) {

		if (!vertices.isEmpty())
			writeIntoFile(outputFilePath, true);
	}

	/**
	 * Method which is used to write into a graphml file and to print the graph
	 * properties on the console simultaneously.
	 * 
	 * @param outputFilePath
	 */
	public void printAndWrite(String outputFilePath) {

		if (!vertices.isEmpty()) {
			saveAllShortestPaths();
			Thread fileWriterThread = new Thread(new Runnable() {
				@Override
				public void run() {
					NetworkGraph.this.writeIntoFile(outputFilePath, false);
				}
			});
			fileWriterThread.start();
			this.print(false);
			try {
				fileWriterThread.join();
			} 
			catch (InterruptedException interruptedException) {
				interruptedException.printStackTrace();
			}
		}
	}
	
	/**
	 * Two network graph objects have the same hash code if they are equal.
	 */
	@Override
	public int hashCode() {

		final int prime = 31;
		int result = 1;
		result = prime * result + numberOfEdges;
		result = prime * result + numberOfVertices;
		result = prime * result + ((vertices == null) ? 0 : vertices.hashCode());		
		if (graphEdges != null) {
		for (Edge edge : graphEdges) {
			result = prime * result +  edge.hashCode();
		}}
		return result;
	}

	/**
	 * Method which checks if different network graph objects can be considered as
	 * equal. Equality is defined as: - Same number of edges - Same number of
	 * vertices - Same vertices (nameId) - Each vertex has the same logical adjacent
	 * vertices with corresponding same weight and edgeId.
	 */
	@Override
	public boolean equals(Object obj) {

		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NetworkGraph other = (NetworkGraph) obj;
		if (numberOfEdges != other.numberOfEdges)
			return false;
		if (numberOfVertices != other.numberOfVertices)
			return false;
		for (Entry<String, Vertex> entry : other.vertices.entrySet()) {
			Vertex otherVertex = entry.getValue();
			String key = entry.getKey();
			//Check if vertex contained in this graph
			if (!this.vertices.containsKey(key))
				return false;
			//Check if adjacent vertices/edges contained in this graph
			LinkedList<Edge> otherVertexEdges = otherVertex.getEdgeList();
			Vertex vertex = this.vertices.get(key);
			LinkedList<Edge> thisVertexEdges = vertex.getEdgeList();
			for (Edge otherEdge : otherVertexEdges) {
				if (!thisVertexEdges.contains(otherEdge))
					return false;
			}
		}
		return true;
	}
	
	/**
	 * Method to provide a String representation of a networkGraph object.
	 * @return Returns The number of nodes, number of edges, the nameIds of the nodes and the
	 * 		   edgeIds
	 */
	@Override
	public String toString() {
		
		return "Number of nodes: " + getNumberOfVertices() + "\nNumber of edges: " + getNumberOfEdges() 
			 + "\nVertices: " + vertices.keySet() + "\nEdges: " + graphEdges;
	}

	/**
	 * Method with default access modifier which passes the vertices LinkedHashMap
	 * so that the VertexNotInGraphException has access to it.
	 * 
	 * @return vertices The vertices of the network graph
	 */
	LinkedHashMap<String, Vertex> getVertices() {
		
		return vertices;
	}

	/**
	 * Method which takes a NetworkGraph object by reference and applies the
	 * Dijkstra algorithm on the graph (From start vertex, to all vertices).
	 * 
	 * @param networkGraph
	 * @param startVertex  From this vertex on Dijkstra is applied
	 */
	private void dijkstra(NetworkGraph networkGraph, Vertex startVertex) {

		networkGraph.initDijkstra();
		PriorityQueue<Vertex> toProceedVertices = new PriorityQueue<Vertex>();
		HashSet<Vertex> proceededVertices = new HashSet<Vertex>();
		startVertex.setDistance(0);
		toProceedVertices.add(startVertex);
		while (!toProceedVertices.isEmpty()) {
			// Get vertex with minimum value (current processed vertex)
			Vertex currentVertex = toProceedVertices.poll();
			for (Edge edge : currentVertex.getEdgeList()) {
				int weight = edge.getWeight();
				Vertex adjacentVertex = edge.getTarget();
				if (!proceededVertices.contains(adjacentVertex)) {
					relax(currentVertex, adjacentVertex, weight);
					toProceedVertices.add(adjacentVertex);
				}
				proceededVertices.add(currentVertex);
			}
		}
	}

	/**
	 * Method to check which path belongs to the shortest path by comparing the the
	 * distances.
	 * 
	 * @param currentVertex  The current proceeded vertex in the Dijkstra algorithm
	 * @param adjacentVertex A vertex which is adjacent to the current proceeded
	 *                       vertex
	 * @param weight         The weight between the edge from currentVertex to
	 *                       adjacentVertex
	 */
	private void relax(Vertex currentVertex, Vertex adjacentVertex, int weight) {

		if (adjacentVertex.getDistance() > currentVertex.getDistance() + weight) {
			adjacentVertex.setDistance(currentVertex.getDistance() + weight);
			adjacentVertex.setPrevVertex(currentVertex);
		}
	}

	/**
	 * Method to add a vertex into a a network graph object.
	 * 
	 * @param newVertex The vertex which will be inserted into the network graph
	 *                  object
	 */
	private void addVertex(Vertex newVertex) {
		
		if (newVertex != null) {
			if (vertices.containsKey(newVertex.getNameId()))
				throw new IllegalArgumentException("Vertex already exist in the graph.(Same nameId)");
			vertices.put(newVertex.getNameId(), newVertex);
			newVertex.setInsertionOrder(numberOfVertices);
			numberOfVertices++;
		}
		else
			throw new NullPointerException("Vertex which shall be added is null");
	}

	/**
	 * Method to get a vertex using the name as key.
	 * 
	 * @throws VertexNotInGraphException
	 */
	private Vertex getVertex(String nameId) throws VertexNotInGraphException {

		if (vertices.containsKey(nameId)) {
			return vertices.get(nameId);
		}
		else {
			throw new VertexNotInGraphException();
		}
	}

	/**
	 * Method which traces the path from startVertex to endVertex 
	 * by using the prevVertex attribute of a vertex.
	 * 
	 * @param startVertex
	 * @param endVertex
	 * @return Returns the traced path from start vertex to end vertex as String
	 */
	private String tracePath(Vertex startVertex, Vertex endVertex) {

		StringBuilder path = new StringBuilder();
		Vertex currentVertex = endVertex;
		Stack<String> stack = new Stack<String>();
		while (currentVertex != null) {
			stack.push(currentVertex.getNameId() + " ");
			if (currentVertex.equals(startVertex))
				break;
			currentVertex = currentVertex.getPrevVertex();
		}
		while (!stack.empty()) {
			path.append(stack.pop());
		}
		return path.toString();
	}

	/**
	 * Method which is used to initialize the graph for the Dijkstra algorithm. The
	 * distance of all vertices is set to Integer.MAX_VALUE and the previous
	 * vertices are set to null.
	 */
	private void initDijkstra() {

		for (Entry<String, Vertex> entry : vertices.entrySet()) {
			Vertex vertex = entry.getValue();
			vertex.setDistance(Integer.MAX_VALUE);
			vertex.setPrevVertex(null);
		}
	}

	/**
	 * Method which is used to initialize the colour for the modified DFS algorithm.
	 */
	private void initConnectivity() {

		for (Entry<String, Vertex> entry : vertices.entrySet()) {
			Vertex vertex = entry.getValue();
			vertex.setColour(Colour.WHITE);
		}
	}

	/**
	 * This method traverses the graph from a given vertex on it uses the same
	 * recursive principle as a deep first search.
	 * 
	 * @param vertex Current proceeded vertex
	 * @return visitedVerticesCounter Amount of vertices which are connected to each
	 *         other from the first passed vertex on
	 */
	private int visitVertex(Vertex vertex, int visitedVerticesCounter) {

		vertex.setColour(Colour.GREY);
		visitedVerticesCounter++;
		for (Edge edge : vertex.getEdgeList()) {
			Vertex adjacentVertex = edge.getTarget();
			if (adjacentVertex.getColour() == Colour.WHITE) {
				// Needed in order to pass the amount of visited vertices to the 'first called' function
				visitedVerticesCounter = visitVertex(adjacentVertex, visitedVerticesCounter);
			}
		}
		vertex.setColour(Colour.BLACK);
		return visitedVerticesCounter;
	}

	/**
	 * Method which initializes the distance matrix so that each vertex has a
	 * distance of 0 to itself or to the distance of the corresponding path if it
	 * exist or to Integer.MAX_VALUE if no direct path exists.
	 * 
	 * @param distMat The distance matrix
	 */
	private void initDistanceMatrix(int distMat[][]) {

		for (int i = 0; i < numberOfVertices; i++) {
			for (int j = 0; j < numberOfVertices; j++) {
				if (i == j)
					distMat[i][j] = 0;
				else
					distMat[i][j] = Integer.MAX_VALUE;
			}
		}
		int i;
		int j;
		// Add edges weights from the network graph to distance matrix
		for (Entry<String, Vertex> entry : vertices.entrySet()) {
			i = entry.getValue().getInsertionOrder();
			LinkedList<Edge> tempEdgeList = entry.getValue().getEdgeList();
			for (Edge tempEdge : tempEdgeList) {
				Vertex currentTarget = tempEdge.getTarget();
				j = currentTarget.getInsertionOrder();
				distMat[i][j] = tempEdge.getWeight();
			}
		}
	}

	/**
	 * Method which applies the Floyd algorithm on the network graph.
	 * 
	 * @param distMat The distance matrix
	 */
	private void floyd(int distMat[][]) {

		initDistanceMatrix(distMat);
		// We consider the paths from i to j with taking an indirection
		for (int indirection = 0; indirection < numberOfVertices; indirection++) {
			for (int i = 0; i < numberOfVertices; i++) {
				// All vertices will be selected as end/destination one after another
				for (int j = 0; j < numberOfVertices; j++) {
					// Check if it is profitable to take the indirection, if yes than update
					if (distMat[i][j] - distMat[indirection][j] > distMat[i][indirection])
						distMat[i][j] = distMat[i][indirection] + distMat[indirection][j];
				}
			}
		}
	}

	/**
	 * This method initializes the count matrix so that if no path exists the cell
	 * will get the value of 0 and if a direct path exists it will get the value of
	 * 1.
	 * 
	 * @param countMat Matrix in which the amount of shortest path is saved
	 */
	private void initCountMatrix(int countMat[][]) {

		int i;
		int j;
		for (Entry<String, Vertex> entry : vertices.entrySet()) {
			i = entry.getValue().getInsertionOrder();
			LinkedList<Edge> tempEdgeList = entry.getValue().getEdgeList();
			for (Edge edge : tempEdgeList) {
				Vertex adjacentVertex = edge.getTarget();
				j = adjacentVertex.getInsertionOrder();
				countMat[i][j] = 1;
			}
		}
	}

	/**
	 * Method which applies the Floyd algorithm and counting the amount of shortest
	 * paths.
	 * 
	 * @param distMat  The distance matrix
	 * @param countMat The matrix which counts the amount of shortest paths of
	 *                 adjacency pairs
	 */
	private void betweennessCentralityFloyd(int distMat[][], int countMat[][]) {

		for (int indirection = 0; indirection < numberOfVertices; indirection++) {
			for (int i = 0; i < numberOfVertices; i++) {
				for (int j = 0; j < numberOfVertices; j++) {
					// Check if it is profitable to take the indirection, if yes than update
					// Case if only one shortest path
					if (distMat[i][j] - distMat[indirection][j] > distMat[i][indirection]) {
						distMat[i][j] = distMat[i][indirection] + distMat[indirection][j];
						countMat[i][j] = countMat[i][indirection] * countMat[indirection][j];
					}
					// Case if more than one shortest path exist
					else if (distMat[i][j] - distMat[i][indirection] == distMat[indirection][j]) {
						countMat[i][j] = countMat[i][j] + countMat[i][indirection] * countMat[indirection][j];
					}
				}
			}
		}
	}

	/**
	 * Method which calculates the betweenness centrality of a vertex.
	 * 
	 * @param distMat  The distance matrix
	 * @param countMat The matrix which counts the amount of shortest paths of
	 *                 adjacency pairs
	 * @param v        Insertion order of the vertex of which the betweenness
	 *                 centrality shall be calculated
	 * @return Returns the betweenness centrality of a vertex which is rounded by
	 *         two decimals
	 */
	private double calculateBetweennessCentrality(int distMat[][], int countMat[][], int v) {

		double result = 0;
		// Iterate over lower triangular matrix since matrix is symmetric.
		for (int s = 0; s < numberOfVertices - 1; s++) {
			for (int t = s + 1; t < numberOfVertices; t++) {
				// We consider all paths from s to t, s!=t!=v
				if (s != v && s != t && v != t) {
					// Get the amount of shortest path in which the vertex with insertion order v is included
					double sigmaStV;
					if (distMat[s][t] - distMat[v][t] < distMat[s][v])
						sigmaStV = 0;
					else
						sigmaStV = countMat[s][v] * countMat[v][t];
					double sigmaSt = countMat[s][t];
					result += sigmaStV / sigmaSt;
				}
			}
		}
		return Math.round(result * 100.0) / 100.0;
	}

	/**
	 * Method fills the distance and count matrices for the output stream and file
	 * writing.
	 * 
	 * @param distMat
	 * @param countMat
	 */
	private void calculateTables(int[][] distMat, int[][] countMat) {

		initDistanceMatrix(distMat);
		initCountMatrix(countMat);
		betweennessCentralityFloyd(distMat, countMat);
	}

	/**
	 * Method which is used to print the graph properties on the console.
	 * 
	 * @param calculateAllPaths Variable to indicate whether all shortest paths have
	 *                          to be calculated or not
	 */
	private void print(boolean calculateAllPaths) {

		if (calculateAllPaths)
			saveAllShortestPaths();
		System.out.println("#Graph information#");
		System.out.println(this.toString());
		System.out.println("###Edges information###");
		for (Edge edge : graphEdges) {
			System.out.println("	EdgeId: " + edge + ", weight: " + edge.getWeight());
		}
		if (this.isConnected()) {
			System.out.println("	Graph is connected!");
			System.out.println("	Diameter: " + this.calculateDiameter());
		} 
		else {
			System.out.println("	Graph is not connected!");
			System.out.println("	Diameter: infinity");
		}
		System.out.println("###Shortest paths (considering transitivity)###");
		String prevLineNameId = null;
		for (ShortestPath path : shortestPaths) {
			String sourceNameId = path.getSource().getNameId();
				if (!sourceNameId.equals(prevLineNameId))
					System.out.println("Source node " + sourceNameId + ":");
				System.out.println("	To node " + path.getTarget() + ":");
				System.out.print("	path-> " + path + "; length ->" + path.getLength());
				System.out.println();
			prevLineNameId = sourceNameId;
		}
		System.out.println("###Betweenness centrality###");
		int V = this.getNumberOfVertices();
		int distMat[][] = new int[V][V];
		int countMat[][] = new int[V][V];
		calculateTables(distMat, countMat);
		double result;
		for (Entry<String, Vertex> entry : vertices.entrySet()) {
			Vertex currentVertex = entry.getValue();
			result = this.calculateBetweennessCentrality(distMat, countMat, currentVertex.getInsertionOrder());
			System.out.println("	Node " + currentVertex.getNameId() + ": " + result);
		}
	}

	/**
	 * Method to write graph attributes (connectivity, amount of vertices and edges
	 * and the diameter) into a graphml file.
	 * 
	 * @param printWriter used to write a formatted text into a file
	 */
	private void writeGraphAttributesIntoFile(PrintWriter printWriter) {

		String connectivity;
		int amountEdges = this.getNumberOfEdges();
		int diameter = this.calculateDiameter();
		if (this.isConnected()) {
			connectivity = "connected";
			printWriter.printf("<graph id=\"G\" edgedefault=\"undirected\" "
					+ "connectivity=\"%s\" diameter=%d numberOfNodes=%d " + "amountOfEdges=%d>\n", connectivity,
					diameter, numberOfVertices, amountEdges);
		} 
		else {
			connectivity = "not connected";
			printWriter.printf(
					"<graph id=\"G\" edgedefault=\"undirected\" "
							+ "connectivity=\"%s\" diameter=\"infinity\" numberOfNodes=%d " + "amountOfEdges=%d>\n",
					connectivity, numberOfVertices, amountEdges);
		}
	}

	/**
	 * Method to write the vertices into a graphml file with the attributes.
	 * 
	 * @param printWriter used to write a formatted text into a file
	 */
	private void writeVerticesIntoFile(PrintWriter printWriter) {

		int V = this.getNumberOfVertices();
		int distMat[][] = new int[V][V];
		int countMat[][] = new int[V][V];
		calculateTables(distMat, countMat);
		for (Entry<String, Vertex> entry : vertices.entrySet()) {
			Vertex vertex = entry.getValue();
			double BetwennessCentrality = this.calculateBetweennessCentrality(distMat, countMat,
					vertex.getInsertionOrder());
			printWriter.printf(
					"    <node id=\"%s\">\n" + "      <data key=\"v_insertionOrder\">%d</data>\n"
							+ "      <data key=\"v_bc\">%.2f</data>\n    </node>\n",
					vertex.getNameId(), vertex.getInsertionOrder(), BetwennessCentrality);
		}
	}

	/**
	 * Method to write the edges attributes into a graphml.
	 * 
	 * @param printWriter used to write a formatted text into a file
	 */
	private void writeEdgesIntoFile(PrintWriter printWriter) {
		
		for (Edge edge : graphEdges) {
			printWriter.printf(
					"    <edge source=\"%s\" target=\"%s\">\n" + "      <data key=\"e_id\">%d</data>\n"
							+ "      <data key=\"e_weight\">%d</data>\n" + "    </edge>\n",
					edge.getSource(), edge.getTarget(), edge.getEdgeId(), edge.getWeight());
		}
	}

	/**
	 * Method to write the shortest paths of each pair of vertices into a graphml
	 * file. Considers transitivity of a path.
	 * 
	 * @param printWriter used to write a formatted text into a file
	 */
	private void writeShortestPathsIntoFile(PrintWriter printWriter) {

		for (ShortestPath path : shortestPaths) {
				printWriter.printf("    <ShortestPath source=\"%s\" target=\"%s\">\n", path.getSource(),
						path.getTarget());
				printWriter.printf("      <data key=\"g_path\">%s</data>\n", path.getPath());
				printWriter.printf("      <data key=\"path_length\">%d</data>\n", path.getLength());
				printWriter.print("    </ShortestPath>\n");
		}
	}

	/**
	 * Method to write all graph properties into a graphml file.
	 * 
	 * @param outputFilePath    The file path where the graph properties are written
	 * @param calculateAllPaths Variable to indicate whether all shortest paths have
	 *                          to be calculated or not
	 */
	private void writeIntoFile(String outputFilePath, boolean calculateAllPaths) {

		if (calculateAllPaths)
			saveAllShortestPaths();
		FileWriter fileWriter;
		try {
			fileWriter = new FileWriter(outputFilePath);
			PrintWriter printWriter = new PrintWriter(fileWriter);
			String encoding = Charset.defaultCharset().toString();
			String header = "<?xml version=\"1.0\" encoding=\"" + encoding + "\"?>\n"
					+ "<graphml xmlns=\"http://graphml.graphdrawing.org/xmlns\"\n"
					+ "         xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n"
					+ "         xsi:schemaLocation=\"http://graphml.graphdrawing.org/xmlns\n"
					+ "         http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd\">\n"
					+ "<!-- Created by JavaGroup6 Network Graph analysis program -->\n"
					+ "  <key id=\"v_insertionOrder\" for=\"node\" attr.name=\"id\" attr.type=\"int\"/>\n"
					+ "  <key id=\"v_bc\" for=\"node\" attr.name=\"bc\" attr.type=\"double\"/>\n"
					+ "  <key id=\"e_id\" for=\"edge\" attr.name=\"id\" attr.type=\"int\"/>\n"
					+ "  <key id=\"e_weight\" for=\"edge\" attr.name=\"weight\" attr.type=\"int\"/>\n"
					+ "  <key id=\"g_path\" for=\"ShortestPath\" attr.name=\"path\" attr.type=\"String\"/>\n"
					+ "  <key id=\"path_length\" for=\"ShortestPath\" attr.name=\"length\" attr.type=\"int\"/>\n";
			printWriter.print(header);
			this.writeGraphAttributesIntoFile(printWriter);
			this.writeVerticesIntoFile(printWriter);
			this.writeEdgesIntoFile(printWriter);
			this.writeShortestPathsIntoFile(printWriter);
			printWriter.print("  </graph>\n" + "</graphml>");
			printWriter.close();
		} 
		catch (IOException ioException) {
			ioException.printStackTrace();
		}
	}

	/**
	 * Method to save all ShortestPaths objects.
	 */
	private void saveAllShortestPaths() {

		shortestPaths = new LinkedList<ShortestPath>();
		for (Entry<String, Vertex> startEntry : vertices.entrySet()) {
			Vertex start = startEntry.getValue();
			dijkstra(this, start);
			for (Entry<String, Vertex> endEntry : vertices.entrySet()) {
				Vertex end = endEntry.getValue();
				if (start.getInsertionOrder() < end.getInsertionOrder()) {
					if (end.getDistance() != Integer.MAX_VALUE)
						shortestPaths.add(new ShortestPath(start, end, tracePath(start, end), end.getDistance()));
				}
			}
		}
	}
}