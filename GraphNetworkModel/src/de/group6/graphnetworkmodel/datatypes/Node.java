package de.group6.graphnetworkmodel.datatypes;

/**
 * A basic Node class which will be used to extend Vertex class. The class is
 * used in order to differentiate between a topological node and a vertex with
 * attributes concerning the required algorithms.
 * 
 * @author JavaGroup6
 */
class Node {

	private String nameId;

	Node(String nameId) {

		this.nameId = nameId;
	}

	/**
	 * Two node objects have the same hash code if they are equal.
	 */
	@Override
	public int hashCode() {

		final int prime = 31;
		int result = 1;
		result = prime * result + ((nameId == null) ? 0 : nameId.hashCode());
		return result;
	}

	/**
	 * Since each vertex of a NetworkGraph object is unique by its nameId, the
	 * equals method defines the equality of nodes by the nameId.
	 */
	@Override
	public boolean equals(Object obj) {

		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Node other = (Node) obj;
		if (nameId == null) {
			if (other.nameId != null)
				return false;
		} 
		else if (!nameId.equals(other.nameId))
			return false;
		return true;
	}

	@Override
	public String toString() {

		return nameId;
	}
	
	String getNameId() {

		return nameId;
	}

	void setNameId(String nameId) {

		this.nameId = nameId;
	}
}