package de.group6.graphnetworkmodel.main;

import de.group6.graphnetworkmodel.datatypes.NetworkGraph;
import de.group6.graphnetworkmodel.graphmlparser.GraphmlParser;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.logging.Logger;

/**
 * Main command line interface program. User input is obtained and verified. 
 * 
 * @author JavaGroup6
 */
public class MainCLI {

	private final static Logger mainLOG = Logger.getLogger(MainCLI.class.getName());

	public static void main(String[] args) {
		
		long startTime = System.nanoTime();
		System.out.println("-----Graph Network Model Program-----");
		mainLOG.info("Size of args[]:" + args.length);
		if (!(args.length == 0)) {
			NetworkGraph networkGraph = new NetworkGraph();
			String inputFilePath = args[0];
			inputFilePath = verifyUserInput(inputFilePath);
			GraphmlParser myGraphmlParser = new GraphmlParser(networkGraph, inputFilePath);
			myGraphmlParser.mapVerticesAndEdgesFromFile();
			//Case if only file is provided -> print all graph properties
			if (args.length == 1)
				networkGraph.print();
			else {
				mainLOG.info("Passed command: " + args[1]);
				StringBuilder arguments = new StringBuilder();
				for (int i = 2; i < args.length; i++)
					arguments.append(args[i]).append(" ");
				mainLOG.info("Passed argument(s): " + arguments.toString());
				//Case File and Dijkstra command
				if (args[1].equals("-s") && args.length == 4) {
					String startNameId = args[2];
					String endNameId = args[3];
					System.out.println("Shortest path: " + networkGraph.getShortestPath(startNameId, endNameId));
				}
				//Case betweenness calculation
				else if (args[1].equals("-b") && args.length == 3) {
					String switchNameId = args[2];
					System.out.println("Betweenneess centrality of vertex: " + networkGraph.betweennessCentrality(switchNameId));
				}
				//Case calculate all properties and make an output graphml file
				else if (args[1].equals("-a") && args.length == 3) {
					String outputFilePath = args[2]; 
					outputFilePath = checkOutputFilePath(inputFilePath, outputFilePath);
					mainLOG.info("Output file path: " + args[2]);
					networkGraph.printAndWrite(outputFilePath);
				} 
				else
					System.err.println("Unknown argument or wrong options, please restart program!");
			}
		} 
		else
			System.err.println("File path and argument have to be provided, please restart program!");
		long endTime = System.nanoTime();
		mainLOG.info("Program runtime in miliseconds: " + ((endTime - startTime) / 1000000));
	
	}
	
	/**
	 * Method to rename an existing file to prevent a file from being overwritten.
	 * 
	 * @param outputFilePath
	 * @return Returns a new file name.
	 */
	private static String renameFileName(String outputFilePath) {
		
		File outputFile = new File(outputFilePath);
		String string = null;
		int indexOfDot;
		boolean fileExists = false;
		do {
			if (outputFile.exists()) {
				indexOfDot = outputFilePath.lastIndexOf(".");
				string = outputFilePath.substring(0, indexOfDot);
				outputFilePath = string.concat("-copy.graphml");
				outputFile = new File(outputFilePath);
				fileExists = false;
			} 
			else
				fileExists = true;
		} while (fileExists == false);
		return outputFilePath;
	}

	/**
	 * Method which checks if the output file exists and if it has the data extension .graphml.
	 * 
	 * @param inputFilePath  The file path from which we import the vertices and edges.
	 * @param outputFilePath The file path in which we store the graph properties.
	 */
	private static String checkOutputFilePath(String inputFilePath, String outputFilePath) {

		if (!outputFilePath.endsWith(".graphml")) {
			outputFilePath = outputFilePath.concat(".graphml");
			mainLOG.info("Data extension changed to graphml");
		} 
		else {
			if (outputFilePath.equals(inputFilePath)) {
				String string = null;
				int indexOfDot = outputFilePath.lastIndexOf(".");
				string = outputFilePath.substring(0, indexOfDot);
				outputFilePath = string.concat("-copy.graphml");
			}
		}
		File outputFile = new File(outputFilePath);
		if (outputFile.exists())
			return renameFileName(outputFilePath);
		return outputFilePath;
	}
	
	/**
	 * Method which used to verify the input file path in order to handle FileNotFound, FileNotReadable and file extension 
	 * not graphml exception.
	 * 
	 * @param inputFilePath The file path from which we import the vertices and edges.
	 */
	@SuppressWarnings("resource")
	private static String verifyUserInput(String inputFilePath) {

		Scanner userInput = new Scanner(System.in);
		Path path = Paths.get(inputFilePath);
		while (!Files.exists(path) || !Files.isReadable(path) || !inputFilePath.endsWith(".graphml")) {
			if (!Files.exists(path))
				System.out.println("File does not exists!");
			if (!Files.isReadable(path))
				System.out.println("File is not readable!");
			if (!inputFilePath.endsWith(".graphml"))
				System.out.println("File does not end with .graphml!");
			String userAnswer;
			do {
				System.out.println("Do you want to provide another file path (write yes or no)? ");
				userAnswer = userInput.nextLine();
				if (userAnswer.equalsIgnoreCase("yes")) {
					System.out.print("Provide a new file path >");
					String pathStr = userInput.nextLine();
					System.out.println("You wrote: " + pathStr);
					path = Paths.get(pathStr);
					inputFilePath = path.toString();
				} 
				else if (userAnswer.equalsIgnoreCase("no")) {
					System.out.println("Terminate program");
					System.exit(1);
				}
			} while (!userAnswer.equalsIgnoreCase("yes") && !userAnswer.equalsIgnoreCase("no"));
		}
		return inputFilePath;
	}
}