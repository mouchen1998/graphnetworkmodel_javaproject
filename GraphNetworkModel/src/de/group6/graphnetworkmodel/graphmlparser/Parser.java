package de.group6.graphnetworkmodel.graphmlparser;

import de.group6.graphnetworkmodel.datatypes.NetworkGraph;
/**
 *Abstract class of a parser which maps information of a file
 *to a NetworkGraph object. 
 * @author JavaGroup6
 */
public abstract class Parser {

	private NetworkGraph networkGraph;
	private String inputFilePath; 
	
	/**
	 * The parser will be initialized with a reference to a network graph and the
	 * input file path in String representation.
	 * @param networkGraph
	 * @param inputFilePath
	 */
	public Parser(NetworkGraph networkGraph, String inputFilePath){
	
		this.networkGraph = networkGraph;
		this.inputFilePath = inputFilePath;
	}
	
	public NetworkGraph getNetworkGraph() {
		
		return networkGraph;
	}

	public String getInputFilePath() {
		
		return inputFilePath;
	}

	public void setNetworkGraph(NetworkGraph networkGraph) {
		
		this.networkGraph = networkGraph;
	}

	public void setInputFilePath(String inputFilePath) {
		
		this.inputFilePath = inputFilePath;
	}

	/**
	 * Abstract method which has to be overridden, depending on the file type
	 * which contains the graph properties. 
	 */
	public abstract void mapVerticesAndEdgesFromFile();
}