package de.group6.graphnetworkmodel.graphmlparser;

import de.group6.graphnetworkmodel.datatypes.NetworkGraph;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Logger;

/**
 * This class is used to map the vertices and edges from a graphml file into a
 * NetworkGraph object.
 *
 * @author JavaGroup6
 */
public class GraphmlParser extends Parser {

	private final static Logger parserLog = Logger.getLogger(GraphmlParser.class.getName());

	public GraphmlParser(NetworkGraph networkGraph, String inputFilePath) {

		super(networkGraph, inputFilePath);
		parserLog.info("Starting graphml parser");
	}

	/**
	 * Method to get the attribute nameId of a vertex from the currently observed
	 * line of a graphml file.
	 * 
	 * @param currentLine
	 * @return Returns the attribute value nameId of a vertex
	 */
	private String getVertexId(String currentLine) {

		int begin = currentLine.indexOf("\"");
		int end = currentLine.lastIndexOf("\"");
		return currentLine.substring(begin + 1, end);
	}

	/**
	 * Method to get the attribute nameId from a target vertex.
	 *
	 * @param currentLine The currently observed line from a graphml file
	 * @return Returns nameId of target vertex which is an attribute of an edge
	 */
	private String getEdgeAttributeSource(String currentLine) {

		int begin = currentLine.indexOf("\"");
		int end = currentLine.indexOf("\"", begin + 1);
		return currentLine.substring(begin + 1, end);
	}

	/**
	 * Method to get the attribute nameId from a target vertex.
	 * 
	 * @param currentLine The currently observed line from a graphml file
	 * @return Returns nameId of target vertex which is an attribute of an edge
	 */
	private String getEdgeAttributeTarget(String currentLine) {

		int lastIndexOfTarget = currentLine.lastIndexOf("target");
		int begin = currentLine.indexOf("\"", lastIndexOfTarget);
		int end = currentLine.indexOf("\"", begin + 1);
		return currentLine.substring(begin + 1, end);
	}

	/**
	 * Method to get the attribute edgeId of an edge.
	 * 
	 * @param currentLine The currently observed line from the graphml file
	 * @return Returns the attribute edgeId of an edge
	 */
	private int getEdgeAttributeEdgeId(String currentLine) {

		int begin = currentLine.indexOf(">");
		int end = currentLine.lastIndexOf("<");
		String edgeId = currentLine.substring(begin + 1, end);
		return Integer.parseInt(edgeId.toString());
	}

	/**
	 * Method to get the attribute weight of an edge.
	 * 
	 * @param currentLine The currently observed line from a graphml file
	 * @return Returns the attribute weight of an edge
	 */
	private int getEdgeAttributeWeight(String currentLine) {

		int begin = currentLine.indexOf(">");
		int end = currentLine.lastIndexOf("<");
		String weight = currentLine.substring(begin + 1, end);
		return Integer.parseInt(weight.toString());
	}

	/**
	 * Method to read a graphml file line by line and map these information to a network graph object.
	 * 
	 * @param networkGraph The NetworkGraph object in which a graphml file is mapped
	 * @param filePath     The file path of the file from which we import the
	 *                     vertices and edges
	 */
	@Override
	public void mapVerticesAndEdgesFromFile() {

		BufferedReader input = null;
		NetworkGraph networkGraph = getNetworkGraph();
		String toRecognizeVertex = "node id";
		String toRecognizeEdge = "edge source";
		String breakCondition = "</graph>";
		try {
			input = new BufferedReader(new FileReader(getInputFilePath()));
			String currentLine = input.readLine();
			String vertexId = null;
			String sourceVertexId = null;
			String targetVertexId = null;
			int edgeId = 0;
			int weight = 0;
			while (!currentLine.contains(breakCondition)) {
				if (currentLine.contains(toRecognizeVertex)) {
					vertexId = getVertexId(currentLine);
					networkGraph.addVertex(vertexId);
					input.readLine();
					input.readLine();
				} 
				else if (currentLine.contains(toRecognizeEdge)) {
					sourceVertexId = getEdgeAttributeSource(currentLine);
					targetVertexId = getEdgeAttributeTarget(currentLine);
					currentLine = input.readLine();
					edgeId = getEdgeAttributeEdgeId(currentLine);
					currentLine = input.readLine();
					weight = getEdgeAttributeWeight(currentLine);
					if (weight < 0) {
						System.err.println("The weight of the edge with id " + edgeId + " is negative.");
						System.err.println("Please provide a new graphml file and restart the program.");
						System.err.println("Terminating the program...");
						System.exit(1);
					}
					networkGraph.addEdge(sourceVertexId, targetVertexId, edgeId, weight);
					input.readLine();
				}
				currentLine = input.readLine();
			}
		} 
		catch (FileNotFoundException fileNotFoundException) {
			fileNotFoundException.printStackTrace();
		} 
		catch (IOException ioException) {
			ioException.printStackTrace();
		}
		catch (NullPointerException nullPointerException)
		{
			nullPointerException.printStackTrace();
			System.err.println("Provided file path string is null or file is empty!");
			System.err.println("Terminating program...");
		}
		finally {
			if (input != null) {
				try {
					input.close();
				} 
				catch (IOException ioException) {
					ioException.printStackTrace();
				}
			}
		}
	}
}